package fr.xebia.boxtop.helpers;

import android.app.Activity;
import android.widget.Toast;

public class PrintToastHelper {
	
	public PrintToastHelper () {}
	
	public static void printToast(Activity activity, String message) {
		Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
	}
}
