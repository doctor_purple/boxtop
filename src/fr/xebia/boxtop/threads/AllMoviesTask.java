package fr.xebia.boxtop.threads;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Spinner;
import fr.xebia.boxtop.activities.ItemListActivity;
import fr.xebia.boxtop.helpers.JsonHelper;
import fr.xebia.boxtop.models.Movie;

public class AllMoviesTask extends AsyncTask<String, String, ArrayList<Movie>> {
	ArrayList<Movie> movies = new ArrayList<Movie>();
	Exception getAllMoviesException = null; 
	Context context;
	Activity activity;
	
	
	public AllMoviesTask (Activity a) {
		this.activity = a;
		this.context = activity.getApplicationContext();
		
	}

	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
        
	}

	@Override
	protected ArrayList<Movie> doInBackground(String... params) {
		//In this async method, I use the JSONHelper to send the GET request, parse the results in java objects and
		//return the array of Movies, calles movies. 
		
		try {
			 return JsonHelper.getMovies(params[0]); //http get method
			 	 
		} catch (Exception e) {
			getAllMoviesException = e; //overwrite the exception
			return null;
		} 
		
	}
	
	@Override
	protected void onPostExecute(ArrayList<Movie> movies) {
		this.movies = movies; //this allows the get() method when I call execute()
		
				
	}
}
