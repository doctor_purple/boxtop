package fr.xebia.boxtop.fragments;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import fr.xebia.boxtop.R;
import fr.xebia.boxtop.activities.ItemDetailActivity;
import fr.xebia.boxtop.activities.ItemListActivity;
import fr.xebia.boxtop.adapters.ImageMovieAdapter;
import fr.xebia.boxtop.helpers.NormalizerHelper;
import fr.xebia.boxtop.helpers.PrintToastHelper;
import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.threads.AllMoviesTask;
import fr.xebia.boxtop.threads.DownloadImageTask;
import fr.xebia.boxtop.values.GlobalVariables;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
 
    public static final String ARG_ITEM_ID = "item_id";
    private Movie selectedMovie;
    private ArrayList<Movie> similarMovies;
    private PopupWindow popup;
    private Button buttonClosingPopup;
    private View rootView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setHasOptionsMenu(true);
        

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            selectedMovie = (Movie)getArguments().getSerializable(ARG_ITEM_ID);
        }
    }
    
    

    @Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.activity_details_option, menu);
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        
    	//VIEW ELEMENT
    	rootView = inflater.inflate(R.layout.fragment_item_detail, container, false);
    	rootView.setBackgroundResource(R.color.clouds); //background of the fragment
        
    	//VIEW WIDGET
    	//TextView titleTextView = (TextView) rootView.findViewById(R.id.item_title);
        TextView dateTextView = (TextView) rootView.findViewById(R.id.item_date);
        RatingBar criticsRatingsBar = (RatingBar) rootView.findViewById(R.id.item_critics_ratingsBar);
        RatingBar audienceRatingsBar = (RatingBar) rootView.findViewById(R.id.item_audience_ratingsBar);
        TextView castingTextView = (TextView) rootView.findViewById(R.id.item_cast);
        TextView synopisTextView = (TextView) rootView.findViewById(R.id.item_synopis);
        ImageView posterImageView = (ImageView) rootView.findViewById(R.id.item_poster);
        GridView gridView = (GridView) rootView.findViewById(R.id.item_gridView);
        ProgressBar posterImageSpinner = (ProgressBar) rootView.findViewById(R.id.poster_spinner);
        LinearLayout scrollViewLayoutContainer = (LinearLayout) rootView.findViewById(R.id.item_scrollview_container);
        LinearLayout synopisLayoutContainer = (LinearLayout) rootView.findViewById(R.id.item_synopis_container);
        
        
         
        //FILL THE VIEW ELEMENTS
        // Show the dummy content as text in a TextView.
        if (selectedMovie != null) {
           getActivity().getActionBar().setTitle(selectedMovie.getTitle());
           //titleTextView.setText();
           dateTextView.setText(selectedMovie.getRelease_dates().getTheater());
           criticsRatingsBar.setRating(NormalizerHelper.normalizer(100, 5, selectedMovie.getRatings().getCritics_score()));
           audienceRatingsBar.setRating(NormalizerHelper.normalizer(100, 5, selectedMovie.getRatings().getAudience_score()));  
           
           if (!selectedMovie.getSynopsis().isEmpty()) {
        	   synopisTextView.setText(selectedMovie.getSynopsis());
           	   synopisTextView.setMovementMethod(new ScrollingMovementMethod());
           } else {
        	   synopisLayoutContainer.removeAllViews();
           }
           	 
           castingTextView.setText(NormalizerHelper.buildCastingString(selectedMovie));
           
           //I use the downloader of image to fill the image view   
   			new DownloadImageTask(posterImageView, posterImageSpinner)
   				.execute(selectedMovie.getPosters().getOriginal());  
   			
   			//ASYNCTASK FOR THE SIMILAR MOVIES
   			try {
   				String similar_movie_url = GlobalVariables.URL_SIMILAR_MOVIES + selectedMovie.getId() + GlobalVariables.URL_SIMILAR_JSON;
   				Log.d("TAG", similar_movie_url);
				similarMovies = new AllMoviesTask(getActivity())
					.execute(new String [] {similar_movie_url})
					.get();
			} catch (InterruptedException e) {
				PrintToastHelper.printToast(getActivity(), e.getMessage());
			} catch (ExecutionException e) {
				PrintToastHelper.printToast(getActivity(), e.getMessage());
			}
   			
   			if (similarMovies != null && similarMovies.size() > 0) {
   				//GRIDLAYOUT ADAPTER
   				gridView.setAdapter(new ImageMovieAdapter(getActivity(), similarMovies));
   			} else {
   				//remove the section if there are not similar movies
   				scrollViewLayoutContainer.removeAllViews();
   			}
        }

        return rootView;
    }
	
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------WIDGET LISTENER ON ACTION BAR -------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------- 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
        switch (item.getItemId()) {
        
        case R.id.action_comment:
            initiatePopupWindow(); //when the edit action bar widget is clicked, I created a popupwindow 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
		
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------POPUPWINDOW -------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------- 
	private void initiatePopupWindow() {
		try {
		// We need to get the instance of the LayoutInflater
			LayoutInflater inflater = (LayoutInflater)getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.popup_comment, null);
		popup = new PopupWindow(layout, rootView.getWidth()- 40, 600, true);
		popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
		
		//rating enabled
		RatingBar ratingBar = (RatingBar) layout.findViewById(R.id.popup_myRating);
		ratingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating,
					boolean fromUser) {
				//TODO: save the rating value
				
			}
		});
		
		
		//on closing button I remove the popup
		buttonClosingPopup = (Button) layout.findViewById(R.id.button_close_popup);
		buttonClosingPopup.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				popup.dismiss();
				
			}
		});

		} catch (Exception e) {
			PrintToastHelper.printToast(getActivity(), e.getMessage());
		}
	}
		
	
	
	
}
