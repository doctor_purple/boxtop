package fr.xebia.boxtop.adapters;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import fr.xebia.boxtop.R;
import fr.xebia.boxtop.loaders.ImageLoader;
import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.threads.DownloadImageTask;

public class MoviesListAdapter extends BaseAdapter implements Filterable{
	Context context;
	ArrayList<Movie> movies;
	ArrayList<Movie> copiedMovies; //keep a copy of the movies
	LayoutInflater inflater;
	ImageLoader imageLoader;
	
	TextView titleTextView, yearTextView;
	ImageView postImageView;
	ProgressBar imageSpinner;
	
	//constructor of the adapter
	public MoviesListAdapter (Context context, ArrayList<Movie> movies) {
		this.context = context;
		this.movies = movies;
		this.copiedMovies = movies;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); //allows to call and use the view from this class
		
		// Create ImageLoader object to download and show image in list
        // Call ImageLoader constructor to initialize FileCache
        imageLoader = new ImageLoader(this.context);
	}
	
	
	@Override
	public int getCount() {
		return movies.size();
	}

	@Override
	public Object getItem(int position) {
		return movies.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	
	
	/*
	 * In this method I model the single view that will be replicated for all the items in the list
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		Movie currentMovie = movies.get(position); //get the movie at the current position
		
		if (v == null)
			v = inflater.inflate(R.layout.movie_item, null);
		
		//Change the backgorund for the even rows
		if ((position & 1) == 0) { //checking the last bit, it's faster then modulus
			v.setBackgroundResource(R.color.nephritis);
		} else
			v.setBackgroundResource(R.color.emerald);
		
		
		
		//Inizialite the view element
		titleTextView = (TextView) v.findViewById(R.id.movie_item_title);
		yearTextView = (TextView) v.findViewById(R.id.movie_item_year);
		postImageView = (ImageView) v.findViewById(R.id.movie_item_image);
		imageSpinner = (ProgressBar) v.findViewById(R.id.progressBar);
		
		//set the Title and the year
		titleTextView.setText(currentMovie.getTitle()); 
		yearTextView.setText(""+currentMovie.getYear());
		
		
		//use the caching system
		imageSpinner.setVisibility(View.VISIBLE);
		imageLoader.DisplayImage(currentMovie.getPosters().getProfile(), postImageView);
		imageSpinner.setVisibility(View.GONE);
		
		
//		new DownloadImageTask(postImageView, imageSpinner)
//			.execute(currentMovie.getPosters().getProfile());
		
		
			
		return v;
			
			
	}
	
	/*
	 * This method is used to filter the results, when a search is performed
	 */

	@Override
	public Filter getFilter() {
		Filter mFilter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				if (constraint != null && constraint.length() > 0) {
					constraint = constraint.toString().toLowerCase();
					ArrayList<Movie> filtered_movies = new ArrayList<Movie>(); //copy of the movies 
					
					for (Movie current_movie : copiedMovies) {
						if (current_movie.getTitle().toLowerCase().contains(constraint)) {
							filtered_movies.add(current_movie); //if the string is contained, I add the current_movie to the filtered list
						}
					}
					
					results.count = filtered_movies.size();
					results.values = filtered_movies;
					
				}
				else {
					//when the constraints is not valid anymore, I overwrite the list of movies with the original one  
					results.count = copiedMovies.size();
					results.values = copiedMovies;
				}
				
				return results;
			}

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				if (results != null && results.count > 0) { 
					movies = (ArrayList<Movie>) results.values;
					notifyDataSetChanged(); //notify all the observers that data is changed	
				} else {
					notifyDataSetInvalidated();
				}
			}
			
		};
		return mFilter;
	}

}
