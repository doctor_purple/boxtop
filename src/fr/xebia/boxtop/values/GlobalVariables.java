package fr.xebia.boxtop.values;

public class GlobalVariables {
	
	public static final String URL = "http://xebiamobiletest.herokuapp.com"; //base url of the API
	public static final String URL_ALL_MOVIES = URL + "/api/public/v1.0/lists/movies/box_office.json"; //all movies, with http GET
	public static final String URL_SIMILAR_MOVIES = URL + "/api/public/v1.0/movies/";
	public static final String URL_SIMILAR_JSON = "/similar.json";
	public static final String TAG_MOVIES = "movies";
	public static final String ERROR_CONNECTION = "This application requires an internet connection!";
}
