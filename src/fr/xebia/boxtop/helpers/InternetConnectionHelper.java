package fr.xebia.boxtop.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class InternetConnectionHelper {
	
	public InternetConnectionHelper() {}
	
	public static boolean haveNetworkConnection(Context c) {
	    ConnectivityManager conMgr = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

	    if(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()){
	        return false;
	    }
	return true; 
    }

}
