package fr.xebia.boxtop.fragments;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import fr.xebia.boxtop.R;
import fr.xebia.boxtop.adapters.MoviesListAdapter;
import fr.xebia.boxtop.helpers.InternetConnectionHelper;
import fr.xebia.boxtop.helpers.PrintToastHelper;
import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.threads.AllMoviesTask;
import fr.xebia.boxtop.values.GlobalVariables;
/**
 * A list fragment representing a list of Items. This fragment
 * also supports tablet devices by allowing list items to be given an
 * 'activated' state upon selection. This helps indicate which item is
 * currently being viewed in a {@link ItemDetailFragment}.
 * <p>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ItemListFragment extends ListFragment {

    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";

    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = onMovieItemClickCallbacks;

    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ListView.INVALID_POSITION;

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        public void onItemSelected(Movie m);

    }

    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static Callbacks onMovieItemClickCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(Movie m) {
        }
    };
    
    private ArrayList<Movie> movies;   
    private AllMoviesTask allMovieTask;
    private SearchView searchView;
    private MoviesListAdapter moviesListAdapter;
    private ActionBar actionBar;
    private MenuItem refreshMenuItem;

 

    
    
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemListFragment() {
    }
    
    @Override
	public void onActivityCreated(Bundle savedInstanceState) {
		setHasOptionsMenu(true); //let the fragment know that there is a search menu
		super.onActivityCreated(savedInstanceState);
		
		actionBar = getActivity().getActionBar(); //get the action bar of the activity
		
		
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //here i Have to check that internet is working
        if (!InternetConnectionHelper.haveNetworkConnection(getActivity().getApplicationContext())) {
        	getActivity().finish(); //close the activity and releases resources	
        } else {
        	//ASYNCHRONOUS TASK
            moviesLoader(); //load the movies using asyntask and put them in an adapter
        } 
    }
    
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_menu_action, menu);
        
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView(); //get the search view in the topbar
		searchView.setOnQueryTextListener(searchListener); //append a query listener, it reacts to the changing of the widget
    }
    
    
	@Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        
        //change the background color
        view.setBackgroundResource(R.color.clouds);
        
        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState.getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // Activities containing this fragment must implement its callbacks.
        if (!(activity instanceof Callbacks)) {
            throw new IllegalStateException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = onMovieItemClickCallbacks;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        super.onListItemClick(listView, view, position, id);

        // Notify the active callbacks interface (the activity, if the
        // fragment is attached to one) that an item has been selected.
        //mCallbacks.onItemSelected(DummyContent.ITEMS.get(position).id);
        mCallbacks.onItemSelected((Movie) moviesListAdapter.getItem(position)); //get the object movie when I click
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        getListView().setChoiceMode(activateOnItemClick
                ? ListView.CHOICE_MODE_SINGLE
                : ListView.CHOICE_MODE_NONE);
    }

    private void setActivatedPosition(int position) {
        if (position == ListView.INVALID_POSITION) {
            getListView().setItemChecked(mActivatedPosition, false);
        } else {
            getListView().setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }
  
    
    //-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------ASYNCTASK LOADER TO THE ADAPTER -------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------- 
    private void moviesLoader() {
    	allMovieTask = new AllMoviesTask(getActivity()); //initialize the async task for requiring all the movies
    	if (allMovieTask != null) {
    		try {
            	movies = allMovieTask.execute(new String[] {GlobalVariables.URL_ALL_MOVIES}).get(); //get the movies from the async task   	
            } catch (InterruptedException e) {
            	PrintToastHelper.printToast(getActivity(),e.getMessage());
            } catch (ExecutionException e) {
            	PrintToastHelper.printToast(getActivity(), e.getMessage());
            }
             
    		if (movies != null && movies.size() > 0) {
	            //Build the custom adapter for the movies list
	            moviesListAdapter = new MoviesListAdapter(getActivity(), movies);
	            setListAdapter(moviesListAdapter);
    		}
    	} 	
    } 
    
    
    
    //-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------QUERY LISTENER ON SEARCH -------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------- 
    private String currentQuery = null;
    final private OnQueryTextListener searchListener = new OnQueryTextListener() {
		
		@Override
		public boolean onQueryTextSubmit(String query) {
	        return false;
		}
		
		@Override
		public boolean onQueryTextChange(String newText) {
            if (moviesListAdapter != null) {
            	moviesListAdapter.getFilter().filter(newText);
            }
	           
			return false;
		}
	};

	//-----------------------------------------------------------------------------------------------------------------------------------------
    //--------------------------------------------------WIDGET LISTENER ON ACTION BAR -------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------- 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Take appropriate action for each action item click
        switch (item.getItemId()) {
        
        case R.id.action_refresh:
            // refresh
            refreshMenuItem = item;
            // load the data from server and refresh the adapter
            moviesLoader();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
		
	}
}
