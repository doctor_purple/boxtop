package fr.xebia.boxtop.helpers;

import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.values.GlobalVariables;

public class JsonHelper {
	
	/*
	 * static void constructor
	 */
	public JsonHelper(){};
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------JSON GENERAL METHODS------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	public static ArrayList<Movie> getMovies(String url) throws Exception {
		ArrayList<Movie> movies = new ArrayList<Movie>(); //arraylist of java objects parsed from java
		
		
		
		String JsonString = executeHttpGet(url); //http get method to the link
		Gson gson = new Gson(); //automatic json parser (from google), given a java class which is a mirror
		JsonParser jsonParser = new JsonParser(); //string parser from google gson lib
		JsonObject mainJSONObject = (JsonObject) jsonParser.parse(JsonString); //transform the string into a java JsonObject
		
		if (!mainJSONObject.isJsonNull()) {	
			JsonArray moviesJSON = mainJSONObject.getAsJsonArray(GlobalVariables.TAG_MOVIES);
			
			if (!moviesJSON.isJsonNull()) {
				
				for (JsonElement movieJSON: moviesJSON) {
					Movie movie = gson.fromJson(movieJSON, Movie.class); //the mapper
					movies.add(movie); //movie added to movies list
				}
			}		
		}   
		
		return movies;
		
	}
	
	
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------HTTP GET CONNECTION HANDLER------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	private static String executeHttpGet(String url) throws Exception {
		String JSONString = null;
		//set timeout for the connection	
		DefaultHttpClient httpClient = new DefaultHttpClient();	
		

    	HttpGet httpGet = new HttpGet(url);
    	HttpResponse httpResponse = httpClient.execute(httpGet); //ClientHttp executes the GET request
    	HttpEntity httpEntity = httpResponse.getEntity();
    	if (httpEntity != null) 
    		JSONString = EntityUtils.toString(httpEntity);
        		          
        return JSONString;
	
	}

}
