package fr.xebia.boxtop.threads;

import java.io.InputStream;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class DownloadImageTask extends AsyncTask<String, Void, Bitmap>{
	ImageView bmImage;
	ProgressBar spinner;
	
	public DownloadImageTask(ImageView bmImage, ProgressBar spinner) {
		this.bmImage = bmImage;
		this.spinner = spinner;
	}
	
	
	
	
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		spinner.setVisibility(View.VISIBLE);
	}




	@Override
	protected Bitmap doInBackground(String... params) {
		
		Bitmap mBitmap = null;
		try {
			InputStream in = new URL(params[0]).openStream();
			mBitmap = BitmapFactory.decodeStream(in);
		}
		catch (Exception e) {
			//TODO: load the default Image
		}
		
		return mBitmap;
	}
	
	
	
	protected void onPostExecute(Bitmap result) {
		bmImage.setImageBitmap(result);
		if (spinner!=null && spinner.isEnabled())
			spinner.setVisibility(View.GONE);
	}
}
