package fr.xebia.boxtop.helpers;

import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.models.Movie.Abridged_cast;

public class NormalizerHelper {
	
	public NormalizerHelper() {}
	
	public static float normalizer(int original_range, int new_range, int value) {
		return (value*new_range)/(float)(original_range);
	}
	
	public static String buildCastingString (Movie m) {
		String result ="";
		
		for (int i=0; i < m.getAbridged_cast().size(); i++) {
			if (i != m.getAbridged_cast().size() - 1)
				result += m.getAbridged_cast().get(i).getName() + ", ";
			else
				result += m.getAbridged_cast().get(i).getName();
			
		}
		
		return result;
	}
}
