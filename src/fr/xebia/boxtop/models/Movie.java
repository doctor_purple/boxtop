
package fr.xebia.boxtop.models;

import java.io.Serializable;
import java.util.ArrayList;




public class Movie implements Serializable {

	
	private static final long serialVersionUID = 1807324760762209778L;
	private ArrayList<Abridged_cast> abridged_cast;
   	private Alternate_ids alternate_ids;
   	private String critics_consensus;
   	private String id;
   	private Links links;
   	private String mpaa_rating;
   	private Posters posters;
   	private Ratings ratings;
   	private Release_dates release_dates;
   	private Number runtime;
   	private String synopsis;
   	private String title;
   	private Number year;

 	public ArrayList<Abridged_cast> getAbridged_cast(){
		return this.abridged_cast;
	}
	
 	public Alternate_ids getAlternate_ids(){
		return this.alternate_ids;
	}
	public void setAlternate_ids(Alternate_ids alternate_ids){
		this.alternate_ids = alternate_ids;
	}
 	public String getCritics_consensus(){
		return this.critics_consensus;
	}
	public void setCritics_consensus(String critics_consensus){
		this.critics_consensus = critics_consensus;
	}
 	public String getId(){
		return this.id;
	}
	public void setId(String id){
		this.id = id;
	}
 	public Links getLinks(){
		return this.links;
	}
	public void setLinks(Links links){
		this.links = links;
	}
 	public String getMpaa_rating(){
		return this.mpaa_rating;
	}
	public void setMpaa_rating(String mpaa_rating){
		this.mpaa_rating = mpaa_rating;
	}
 	public Posters getPosters(){
		return this.posters;
	}
	public void setPosters(Posters posters){
		this.posters = posters;
	}
 	public Ratings getRatings(){
		return this.ratings;
	}
	public void setRatings(Ratings ratings){
		this.ratings = ratings;
	}
 	public Release_dates getRelease_dates(){
		return this.release_dates;
	}
	public void setRelease_dates(Release_dates release_dates){
		this.release_dates = release_dates;
	}
 	public Number getRuntime(){
		return this.runtime;
	}
	public void setRuntime(Number runtime){
		this.runtime = runtime;
	}
 	public String getSynopsis(){
		return this.synopsis;
	}
	public void setSynopsis(String synopsis){
		this.synopsis = synopsis;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
 	public Number getYear(){
		return this.year;
	}
	public void setYear(Number year){
		this.year = year;
	}
	
	
	//-----------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------INNER OBJECTS------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------------------------
	public static class Posters implements Serializable {

		
	
		private static final long serialVersionUID = 7976858706351159395L;
		private String thumbnail;
		private String profile;
		private String detailed;
		private String original;
		public String getThumbnail() {
			return thumbnail;
		}
		public void setThumbnail(String thumbnail) {
			this.thumbnail = thumbnail;
		}
		public String getProfile() {
			return profile;
		}
		public void setProfile(String profile) {
			this.profile = profile;
		}
		public String getDetailed() {
			return detailed;
		}
		public void setDetailed(String detailed) {
			this.detailed = detailed;
		}
		public String getOriginal() {
			return original;
		}
		public void setOriginal(String original) {
			this.original = original;
		}
	
	}
	
	public static class Ratings implements Serializable {

		private static final long serialVersionUID = 7575206065479381708L;
		private String critics_rating;
		private int critics_score;
		private String audience_rating;
		private int audience_score;
		public String getCritics_rating() {
			return critics_rating;
		}
		public void setCritics_rating(String critics_rating) {
			this.critics_rating = critics_rating;
		}
		public int getCritics_score() {
			return critics_score;
		}
		public void setCritics_score(int critics_score) {
			this.critics_score = critics_score;
		}
		public String getAudience_rating() {
			return audience_rating;
		}
		public void setAudience_rating(String audience_rating) {
			this.audience_rating = audience_rating;
		}
		public int getAudience_score() {
			return audience_score;
		}
		public void setAudience_score(int audience_score) {
			this.audience_score = audience_score;
		}
	}
	
	public static class Release_dates implements Serializable {

		
	
		private static final long serialVersionUID = 5211532340223601813L;
		private String theater;
		private String dvd;
		public String getTheater() {
			return theater;
		}
		public void setTheater(String theater) {
			this.theater = theater;
		}
		public String getDvd() {
			return dvd;
		}
		public void setDvd(String dvd) {
			this.dvd = dvd;
		}
	}
	
	public static class Links implements Serializable{
		   

		private static final long serialVersionUID = 3692620125619783190L;
		private String alternate;
	   	private String self;

	 	public String getAlternate(){
			return this.alternate;
		}
		public void setAlternate(String alternate){
			this.alternate = alternate;
		}
	 	public String getSelf(){
			return this.self;
		}
		public void setSelf(String self){
			this.self = self;
		}
	}
	
	public static class Alternate_ids implements Serializable{

		private static final long serialVersionUID = 2843857184247522948L;
		private String imdb;

	 	public String getImdb(){
			return this.imdb;
		}
		public void setImdb(String imdb){
			this.imdb = imdb;
		}
	}
	
	public static class Abridged_cast implements Serializable{


		private static final long serialVersionUID = -3665174696473713854L;
		private ArrayList<String> characters;
	   	private String id;
	   	private String name;

	 	public ArrayList<String> getCharacters(){
			return this.characters;
		}
		public void setCharacters(ArrayList<String> characters){
			this.characters = characters;
		}
	 	public String getId(){
			return this.id;
		}
		public void setId(String id){
			this.id = id;
		}
	 	public String getName(){
			return this.name;
		}
		public void setName(String name){
			this.name = name;
		}
	}

}
