package fr.xebia.boxtop.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import fr.xebia.boxtop.R;
import fr.xebia.boxtop.fragments.ItemDetailFragment;
import fr.xebia.boxtop.fragments.ItemListFragment;
import fr.xebia.boxtop.helpers.InternetConnectionHelper;
import fr.xebia.boxtop.helpers.PrintToastHelper;
import fr.xebia.boxtop.models.Movie;
import fr.xebia.boxtop.values.GlobalVariables;



/**
 * An activity representing a list of Movies. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ItemDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link ItemListFragment} and the item details
 * (if present) is a {@link ItemDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link ItemListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class ItemListActivity extends FragmentActivity
        implements ItemListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private static final String TAG = "ItemListActivity";
    
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        
        //network connection running or not
        if (!InternetConnectionHelper.haveNetworkConnection(this.getApplicationContext())) {
        	PrintToastHelper.printToast(this, GlobalVariables.ERROR_CONNECTION);
        	PrintToastHelper.printToast(this, "not connected");
        	
        } else {
        	if (findViewById(R.id.item_detail_container) != null) {
                // The detail container view will be present only in the
                // large-screen layouts (res/values-large and
                // res/values-sw600dp). If this view is present, then the
                // activity should be in two-pane mode.
                mTwoPane = true;

                // In two-pane mode, list items should be given the
                // 'activated' state when touched.
                ((ItemListFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.item_list))
                        .setActivateOnItemClick(true);
            } 
        }     
    }

    /**
     * Callback method from {@link ItemListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(Movie selectedMovie) {
    	Bundle arguments = new Bundle();
        arguments.putSerializable(ItemDetailFragment.ARG_ITEM_ID, selectedMovie);
    	
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.  
            ItemDetailFragment fragment = new ItemDetailFragment();    
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.item_detail_container, fragment)
                    .commit();

        } else {
        	
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, ItemDetailActivity.class);
            detailIntent.putExtras(arguments);
            startActivity(detailIntent);//start the intent, passing the current movie selected
            
        	
        	
        }
    }   
}
